//============================================================================
// Name        : myPacker.cpp
// Author      : 
// Version     :
// Copyright   : Macieg
// Description : Hello World in C++, Ansi-style
//============================================================================

#include "helpers.hpp"
#include "cuboid.hpp"
#include "voxel.hpp"
#include "gridElement.hpp"
#include "grid.hpp"

#include <iostream>
#include <vector>
#include <array>
#include <thread>
#include <algorithm>
#include <cmath>
#include <random>
#include <algorithm>
#include <chrono>
#include <omp.h>


int main() {
	//std::cout << cnl::numeric_limits<fixedType>::max() <<std::endl;
	static_assert(std::max({DIMENSIONS}) + 1 < (double) cnl::numeric_limits<fixedType>::max(),
			"Numbers cannot be represented in current fixed point format.");
	const int tries = TRIES;
	std::array<double,tries> coverages {};
	std::vector<int> dims {DIMENSIONS};
	auto start = std::chrono::steady_clock::now();
	for(int i = 0; i < tries; i++)
	{
		if(DISPLAY_TRIES) std::cout << "Try " << i+1 << std::endl;
		Grid<DIMENSIONS> grid;
		coverages[i] = grid.getPacking(SHOTS,BASIC,BOUNDARY,DEBUG_INFO);
		if(PRINT_POSITIONS) grid.printPositions();
	}
	double sum = std::accumulate(coverages.begin(),coverages.end(),0.0);
	double mean = sum / tries;
	double deviation = std::accumulate(coverages.begin(),coverages.end(),0.0,[mean](double a, double b){ return pow(b - mean,2) + a;});
	deviation = sqrt(deviation/tries);
	std::cout << dims.size() << " dimensional packing. Dimensions: ";
	for(auto dim : dims)
		std::cout << dim << " ";
	std::cout << " Number of trials: " << tries << " Mean coverage: " << mean << " Standard deviation: " << deviation << std::endl;
	auto end = std::chrono::steady_clock::now();
	std::cout << std::chrono::duration<double>(end - start).count() << "s" << std::endl;
	return 0;
}

