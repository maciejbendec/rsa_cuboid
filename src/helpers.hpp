/*
 * helpers.hpp
 *
 *  Created on: Apr 29, 2019
 *      Author: rsteam
 */

#ifndef HELPERS_HPP_
#define HELPERS_HPP_

#include <omp.h>
#include <vector>
#include <array>
#include <cmath>
#include "../cnl/include/cnl/all.h"

using std::vector;
using std::array;
using std::abs;

using indexType = long int;
//using fixedType = cnl::fixed_point<cnl::overflow_integer<int64_t,cnl::saturated_overflow_tag>,-40>;
using fixedType = cnl::fixed_point<int64_t,-DECIMALS>;
constexpr fixedType fixedPointZero {0ll};
constexpr fixedType fixedPointOne  {1ll};
//thanks mate https://stackoverflow.com/a/33158265
template<typename T, std::size_t N>
constexpr T compile_time_accumulator(std::array<T, N> const &A) {
	T sum(T(1));

	for(size_t i = 0; i < N; ++i) {
		sum *= A[i];
	}

	return sum;
}

//TODO make index helper class
template<size_t dimensions>
indexType arrayIndexToInt(array<indexType,dimensions> pos, array<indexType,dimensions> limits){
	indexType index = pos[0];
	indexType multiplier = 1;
	for(size_t i = 1; i < limits.size(); i++){
		multiplier *= limits[i-1];
		index += multiplier * pos[i];
	}
	return index;
}

template<size_t dimensions>
array<indexType,dimensions> intIndexToArray(indexType pos, const array<indexType,dimensions> limits){
	array<indexType,dimensions> arr;
	for(size_t i = 0; i < limits.size(); i++){
		arr[i] = pos % limits[i];
		pos /= limits[i];
	}
	return arr;
}

#endif /* HELPERS_HPP_ */
