/*
 * cuboid.hpp
 *
 *  Created on: Mar 12, 2019
 *      Author: rsteam
 */

#ifndef CUBOID_HPP_
#define CUBOID_HPP_

#include "helpers.hpp"
#include <array>
#include <cmath>
#include <iostream>
template<int dimensions>
class Cuboid{
public:
	Cuboid() = default;
	explicit Cuboid(const std::array<fixedType, dimensions>& pos) :
			position(pos) {
	}
	const std::array<fixedType, dimensions>& getPosition() {
		return position;
	}
	bool isOverlapping(std::array<fixedType, dimensions> pos, const std::array<indexType, dimensions>& limits) {
		bool overlapping = true;

		for (short int dim = 0; dim < dimensions; dim++) {
			if(position[dim] < fixedPointOne && pos[dim] > limits[dim] - fixedPointOne){//periodical neighbour from left
				pos[dim]-= limits[dim];
			}
			else if(position[dim] > limits[dim]-fixedPointOne && pos[dim] < fixedPointOne){//periodical neighbour from right
				pos[dim]+= limits[dim];
			}
			if (cnl::abs(position[dim] - pos[dim]) > fixedPointOne) {
				overlapping = false;
				break;
			}
		}
		return overlapping;
	}
private:
	std::array<fixedType, dimensions> position;
};



#endif /* CUBOID_HPP_ */
