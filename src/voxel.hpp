/*
 * voxel.hpp
 *
 *  Created on: Mar 12, 2019
 *      Author: rsteam
 */

#ifndef VOXEL_HPP_
#define VOXEL_HPP_
#include "helpers.hpp"

#include <vector>
#include <iostream>
#include <memory>

using std::vector;
using std::array;
using std::shared_ptr;

template<size_t dimensions>
class Voxel : public std::enable_shared_from_this<Voxel<dimensions>>{
public:
	explicit Voxel(const array<fixedType, dimensions>& pos) :
	position(pos){
		for (int i = 0; i < dimensions; i++) {
			dimension[i] = fixedPointOne;
		}
		volume = 1;
	}
	explicit Voxel(const array<indexType , dimensions>& pos)
	{
		for(size_t i = 0; i < dimensions; i++){
			position[i] = fixedType(pos[i]);
			dimension[i] = fixedPointOne;
		}
		volume = 1;
	}
	Voxel(std::array<fixedType, dimensions>& pos, array<fixedType, dimensions>& dim) :
	position(pos), dimension(dim) {
		calculateVolume();
	}

	double getVolume() {
		calculateVolume();// TODO verify if needed here
		return volume;
	}
	vector<shared_ptr<Voxel<dimensions>>> splitVoxel(array<fixedType, dimensions> pos, array<indexType,dimensions> limits)
	{ //fuckin' mess
		vector<shared_ptr<Voxel<dimensions>>> voxelsAfterSplit {};
		shared_ptr<Voxel<dimensions>> splittedVoxel = this->shared_from_this();
		for(size_t dim = 0; dim < dimensions; dim++){
			if(position[dim] <= fixedPointOne && pos[dim] >= limits[dim]-fixedPointOne){//periodical neighbour from left
				pos[dim]-= limits[dim];
			}
			else if(position[dim] >= limits[dim]-fixedPointOne && pos[dim] <= fixedPointOne){//periodical neighbour from right
				pos[dim]+= limits[dim];
			}
		}
		if(isBlocking(pos)){
			for(size_t dim=0;dim<dimensions;dim++){
				if(lineInside(pos[dim],dim)){
					std::array<fixedType, dimensions> midpointPosition(splittedVoxel->position);// child voxel will only differ in 1 dimension
					std::array<fixedType, dimensions> midpointDimension(splittedVoxel->dimension);
					if (pos[dim] >= splittedVoxel->position[dim] && pos[dim] - fixedPointOne < splittedVoxel->position[dim] + splittedVoxel->dimension[dim])
					{ //split from "right"
						midpointPosition[dim] = pos[dim] - fixedPointOne;
						splittedVoxel->dimension[dim] = pos[dim] - fixedPointOne - splittedVoxel->position[dim];
						midpointDimension[dim] -= splittedVoxel->dimension[dim];
						auto child = std::make_shared<Voxel<dimensions>>(midpointPosition,midpointDimension);
						voxelsAfterSplit.push_back(splittedVoxel);//start spliting child voxel
						splittedVoxel = child;
					}
					else if (pos[dim] < splittedVoxel->position[dim] && pos[dim] + fixedPointOne >= splittedVoxel->position[dim])
					{ //split from "left"
						midpointPosition[dim] = pos[dim] + fixedPointOne;
						splittedVoxel->dimension[dim] = pos[dim] + fixedPointOne - splittedVoxel->position[dim];
						midpointDimension[dim] -= splittedVoxel->dimension[dim];
						auto child = std::make_shared<Voxel<dimensions>>(midpointPosition,midpointDimension);
						voxelsAfterSplit.push_back(child);//continue splitting voxel
					}
				}
			}
			calculateVolume();
		}
		else{
			voxelsAfterSplit.push_back(this->shared_from_this());
		}
		return voxelsAfterSplit;
	}
	std::array<fixedType, dimensions> position; //position is left edge
	std::array<fixedType, dimensions> dimension;
private:
	void calculateVolume() {
		volume = 1;
		for (size_t i = 0; i < dimensions; i++) {
			volume *= dimension[i];
		}
	}

	bool isBlocking(const std::array<fixedType, dimensions>& pos){
		bool blocking = true;
		for (size_t dim = 0; dim < dimensions; dim++) {
			if (position[dim] >  pos[dim] && position[dim] - pos[dim] >= fixedPointOne)//not blocking from left
			{
				blocking = false;
				break;
			}
			else if (position[dim] < pos[dim] && pos[dim] - position[dim] >= fixedPointOne + dimension[dim])// not blocking from right
			{
				blocking = false;
				break;
			}
		}
		return blocking;
	}

	bool lineInside(fixedType pos,int dim){
		if(pos + fixedPointOne >= position[dim] && pos + fixedPointOne < position[dim]+dimension[dim])//line inside from left
			return true;
		else if(pos - fixedPointOne >= position[dim] && pos - fixedPointOne < position[dim] + dimension[dim])//line inside from right
			return true;
		return false;
	}

	double volume;

	friend class TestVoxelSplit;
};



#endif /* VOXEL_HPP_ */
