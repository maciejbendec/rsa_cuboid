/*
 * grid.hpp
 *
 *  Created on: Mar 21, 2019
 *      Author: rsteam
 */

#ifndef GRID_HPP_
#define GRID_HPP_

#include "cuboid.hpp"
#include "voxel.hpp"
#include "gridElement.hpp"


#include <iostream>
#include <iomanip>
#include <vector>
#include <array>
#include <map>
#include <list>
#include <thread>
#include <algorithm>
#include <cmath>
#include <random>
#include <chrono>
#include <omp.h>

using std::vector;
using std::array;
using std::shared_ptr;
using std::unique_ptr;
using std::make_pair;
using std::cout;
using std::endl;
using std::pair;
using std::map;
using std::list;

template<indexType ... Limits>
class Grid{
protected:
	static constexpr size_t dimensions = sizeof ...(Limits);
	static constexpr array<indexType, dimensions> getLimits(){ return {Limits...};};
	static constexpr size_t length = compile_time_accumulator(getLimits());
public:
	Grid(){
		coverage = 0;
		std::random_device randomDevice;
		for(int i = 0; i < omp_get_max_threads(); i++){
			generators.push_back(std::mt19937 {randomDevice()});
		}
		for (size_t i = 0; i < length; i++) {
			array<indexType,dimensions> iArray = intIndexToArray(i,getLimits());
			elements.push_back(std::make_unique<GridElement<dimensions>>(iArray));
		}
	}
	virtual ~Grid() = default;
	void clearFreeVoxels() {
		freeVoxels.clear();
	}
	void fillFreeVoxels() {
		for(auto& elem : elements){
			vector<shared_ptr<Voxel<dimensions>>> voxels = elem->getVoxels();
			for(auto vox : voxels){
				freeVoxels.push_back(make_pair(arrayIndexToInt(elem->getPosition(),getLimits()),vox));
			}
		}
	}
	void fillFreeVoxelsWithBoundingBoxes() {
		for(auto& elem : elements){
			if(elem->status == GridElementStatus::empty) {
				freeVoxels.push_back(make_pair(arrayIndexToInt(elem->getPosition(), getLimits()), elem->boundingBox));
			}
		}
	}
	void fillNeighbours(){
#pragma omp parallel for
		for (size_t i = 0; i < length; i++) {
			auto n = getNeighbours(intIndexToArray(i,getLimits()),getLimits());
			vector<indexType> neigh= translateNeighbours(n);
#pragma omp critical
			neighbours[i] = neigh;
		}
	}
	vector<double> getProbabilityVector() {
		vector<double> probs;
		for (auto voxel : freeVoxels) {
			probs.push_back(voxel.second->getVolume());
		}
		return probs;
	}

	vector<double> getProbabilityVectorBounding() {
		vector<double> probs;
		for (auto voxel : freeVoxels) {
			probs.push_back(voxel.second->getVolume());
		}
		return probs;
	}

	double getPacking(long int shotsNum, long int basicTries,long int boundingTries,bool debugInfo = false){
		size_t empty=length;
		fillNeighbours();
		for(int i = 0; i < basicTries; i++) { //first, use simple algorithm
			if(debugInfo) cout<<"Basic try " << i+1 << endl;
			empty = generatingStep(shotsNum, true, debugInfo);
		}
		if(boundingTries) analyzeVoxelsAndForget();
		for(int i = 0; i < boundingTries; i++) { //then, bounding box algorithm
			if(debugInfo) cout<<"Bounding box try " << i+1 << endl;
			empty = generatingStepBoundingBox(shotsNum, debugInfo);
		}
		analyzeVoxels();
		while(empty>0){ //then, use complicated and memory hungry one
			empty= generatingStep(shotsNum,false, debugInfo);
		}
		return coverage;
	}
	void printPositions(){
		for(auto& elem : elements){
			if(elem->status == GridElementStatus::filled){
				for(size_t i=0; i< dimensions; i++){
					cout << double(elem->cuboid.getPosition()[i]) << " ";
				}
				cout << endl;
			}
		}
	}

	//voxel based packing, fast but memory intensive for high dimensions or volumes
	size_t generatingStep(long int shotsNum, bool basic, bool debugInfo=false) {
		fillFreeVoxels();
		long numberOfVoxels = freeVoxels.size();
		if(freeVoxels.size() == 0) //failsafe if we already found saturated packing using other algorithms
			return 0;
		if(debugInfo) std::cout<<"Free voxels: " << numberOfVoxels<<std::endl;
		vector<double> probsVector = getProbabilityVector();

		map<indexType,Cuboid<dimensions>> candidates;
		map<indexType,Cuboid<dimensions>> checkedCuboids;

		std::discrete_distribution<> discreteDistribution(probsVector.begin(), probsVector.end());
		std::uniform_real_distribution<> pointDistribution(0,1);

		#pragma omp parallel for
		for (long int shot = 0; shot < shotsNum; shot++) {
			array<fixedType, dimensions> pointDims{};
			auto selectedVoxel = freeVoxels[discreteDistribution(generators[omp_get_thread_num()])];
			for (size_t dim = 0; dim < dimensions; dim++) {
				pointDims[dim] = pointDistribution(generators[omp_get_thread_num()])*selectedVoxel.second->dimension[dim]+selectedVoxel.second->position[dim];
			}
			#pragma omp critical
			candidates[selectedVoxel.first] = Cuboid<dimensions>(pointDims);
		}
		for(auto it = candidates.begin(); it != candidates.end(); it++) {
			auto candidate = *it;
			bool overlapping = false;
			if(elements[candidate.first]->status == GridElementStatus::candidate){
				overlapping = true;
			}
			else{
				auto neigh = neighbours[candidate.first];
				for (auto index : neigh) { // check neighbours
					if(elements[index]->status == GridElementStatus::candidate
					|| (basic && elements[index]->status == GridElementStatus::filled)){
						if(elements[index]->cuboid.isOverlapping(candidate.second.getPosition(), getLimits())){
							overlapping = true;
							break;
						}
					}
				}
			}
			if (!overlapping) {
				checkedCuboids[candidate.first] = candidate.second;
				addChecked(candidate.first,candidate.second);
			}
		}
		for (auto& checked : checkedCuboids) {
			confirmChecked(checked.first);
		}

		if(!basic) analyzeVoxelsPartial(checkedCuboids);
		size_t filled = 0;
		size_t blocked = 0;
		size_t empty = 0;
		for(auto& elem : elements){
		   if (elem->status == GridElementStatus::filled) {
			   filled++;
		   } else if (elem->status == GridElementStatus::blocked) {
			   blocked++;
		   } else {
			   empty++;
		   }
	   }
		if(debugInfo) cout << "FILLED " << filled << " BLOCKED " << blocked << " EMPTY " << empty << endl;
		coverage = (double) filled/(blocked+filled+empty);
		clearFreeVoxels();
		return empty;
	}
	//something between basic and dynamic
	size_t generatingStepBoundingBox(long int shotsNum, bool debugInfo=false) {
		fillFreeVoxelsWithBoundingBoxes();
		long numberOfVoxels = freeVoxels.size();
		if(freeVoxels.size() == 0) //failsafe if we already found saturated packing using other algorithms
			return 0;
		if(debugInfo) std::cout<<"Free bounding boxes: " << numberOfVoxels<<std::endl;
		vector<double> probsVector = getProbabilityVectorBounding();

		map<indexType,Cuboid<dimensions>> candidates;
		map<indexType,Cuboid<dimensions>> checkedCuboids;

		std::discrete_distribution<> discreteDistribution(probsVector.begin(), probsVector.end());
		std::uniform_real_distribution<> pointDistribution(0,1);

#pragma omp parallel for
		for (long int shot = 0; shot < shotsNum; shot++) {
			array<fixedType, dimensions> pointDims{};
			auto selectedVoxel = freeVoxels[discreteDistribution(generators[omp_get_thread_num()])];
			for (size_t dim = 0; dim < dimensions; dim++) {
				pointDims[dim] = pointDistribution(generators[omp_get_thread_num()])*selectedVoxel.second->dimension[dim]+selectedVoxel.second->position[dim];
			}
#pragma omp critical
			candidates[selectedVoxel.first] = Cuboid<dimensions>(pointDims);
		}
		for(auto it = candidates.begin(); it != candidates.end(); it++) {
			auto candidate = *it;
			bool overlapping = false;
			if(elements[candidate.first]->status == GridElementStatus::candidate){
				overlapping = true;
			}
			else{
				auto neigh = neighbours[candidate.first];
				for (auto index : neigh) { // check neighbours
					if(elements[index]->status == GridElementStatus::candidate
					   || (elements[index]->status == GridElementStatus::filled)){
						if(elements[index]->cuboid.isOverlapping(candidate.second.getPosition(), getLimits())){
							overlapping = true;
							break;
						}
					}
				}
			}
			if (!overlapping) {
				checkedCuboids[candidate.first] = candidate.second;
				addChecked(candidate.first,candidate.second);
			}
		}
		for (auto& checked : checkedCuboids) {
			confirmChecked(checked.first);
		}
		analyzeVoxelsAndForgetPartial(checkedCuboids);
		size_t filled = 0;
		size_t blocked = 0;
		size_t empty = 0;
		for(auto& elem : elements){
			if (elem->status == GridElementStatus::filled) {
				filled++;
			} else if (elem->status == GridElementStatus::blocked) {
				blocked++;
			} else {
				empty++;
			}
		}
		if(debugInfo) cout << "FILLED " << filled << " BLOCKED " << blocked << " EMPTY " << empty << endl;
		coverage = (double) filled/(blocked+filled+empty);
		clearFreeVoxels();
		return empty;
	}
protected:
	void addChecked(indexType parentIndex, Cuboid<dimensions>& cuboid ) {
		elements[parentIndex]->cuboid = cuboid;
		elements[parentIndex]->status = GridElementStatus::candidate;
	}
	void confirmChecked(indexType parentIndex) {
		elements[parentIndex]->status = GridElementStatus::filled;
		elements[parentIndex]->clearVoxels();
	}
	void analyzeVoxelsAndForget() {
		#pragma omp parallel for
		for (size_t i = 0; i < length; i++) {
			auto neigh = neighbours[i];
			for (auto index : neigh) {
				if (elements[index]->status == GridElementStatus::filled &&
					elements[i]->status == GridElementStatus::empty) {
					elements[i]->splitVoxels(elements[index]->cuboid, getLimits());
			    }
		    }
			elements[i]->resetBoundingBox();
			double volume = 0;
			for(auto voxel : elements[i]->getVoxels()){
				volume += voxel->getVolume();
				for (size_t a = 0; a < dimensions; a++) {
					if(elements[i]->boundingBox->position[a] > voxel->position[a]){
						elements[i]->boundingBox->dimension[a]+= elements[i]->boundingBox->position[a] - voxel->position[a];
						elements[i]->boundingBox->position[a] = voxel->position[a];
						elements[i]->boundingBox->dimension[a] = std::max(elements[i]->boundingBox->dimension[a], voxel->dimension[a]);
					}
					elements[i]->boundingBox->dimension[a] = std::max(elements[i]->boundingBox->dimension[a] + elements[i]->boundingBox->position[a],
							voxel->dimension[a]+voxel->position[a]) - elements[i]->boundingBox->position[a];
				}
			}
			if(elements[i]->status==GridElementStatus::empty) {
				elements[i]->resetVoxels();
				elements[i]->boundingRatio = volume / elements[i]->boundingBox->getVolume();
			}
	    }
	}

	void analyzeVoxelsAndForgetPartial(const map<indexType,Cuboid<dimensions>> & checkedCuboids) {
		vector<indexType> updateVector;
		for(auto checked : checkedCuboids) {
			auto neigh = neighbours[checked.first];
			for (auto index : neigh) {
				if (elements[index]->status == GridElementStatus::empty) {
					updateVector.push_back(index);
				}
			}
		}
		sort(updateVector.begin(),updateVector.end());
		auto it  = unique(updateVector.begin(),updateVector.end());
		updateVector.resize( std::distance(updateVector.begin(),it) );
#pragma omp parallel for
		for(auto ind = updateVector.begin(); ind < updateVector.end(); ind++)
		{
			auto i = *ind;
			elements[i]->resetBoundingBox();
			double volume = 0;
			auto neigh = neighbours[i];

			for (auto index = neigh.begin(); index < neigh.end(); index++) {
				if (elements[*index]->status == GridElementStatus::filled) {
					elements[i]->splitVoxels(elements[*index]->cuboid, getLimits());
				}
			}
			for (auto voxel : elements[i]->getVoxels()) {
				volume += voxel->getVolume();
				for (size_t a = 0; a < dimensions; a++) {
					if (elements[i]->boundingBox->position[a] > voxel->position[a]) {
						elements[i]->boundingBox->dimension[a] +=
								elements[i]->boundingBox->position[a] - voxel->position[a];
						elements[i]->boundingBox->position[a] = voxel->position[a];
						elements[i]->boundingBox->dimension[a] = std::max(elements[i]->boundingBox->dimension[a],
																		  voxel->dimension[a]);
					}
					elements[i]->boundingBox->dimension[a] =
							std::max(elements[i]->boundingBox->dimension[a] + elements[i]->boundingBox->position[a],
									 voxel->dimension[a] + voxel->position[a]) -
							elements[i]->boundingBox->position[a];
				}
			}
			if (elements[i]->status == GridElementStatus::empty) {
				elements[i]->resetVoxels();
				elements[i]->boundingRatio = volume / elements[i]->boundingBox->getVolume();
			}
		}
	}
	void analyzeVoxels() {
#pragma omp parallel for
		for (size_t i = 0; i < length; i++) {
			auto neigh = neighbours[i];
			for (auto index : neigh) {
				if (elements[index]->status == GridElementStatus::filled &&
					elements[i]->status == GridElementStatus::empty) {
					elements[i]->splitVoxels(elements[index]->cuboid, getLimits());
				}
			}
		}
	}
	void analyzeVoxelsPartial(const map<indexType,Cuboid<dimensions>> & checkedCuboids) {
#pragma omp parallel for
		for (size_t i = 0; i < length; i++) {
			auto neigh = neighbours[i];
			for (auto index : neigh) {
				if (elements[index]->status == GridElementStatus::filled &&
					elements[i]->status == GridElementStatus::empty &&
					checkedCuboids.find(index) != checkedCuboids.end()) {
					elements[i]->splitVoxels(elements[index]->cuboid, getLimits());
				}
			}
		}
	}

	vector<indexType> translateNeighbours(vector<array<indexType,dimensions>> neigh){
		vector<indexType> translated {};
		for(auto a: neigh){
			translated.push_back(arrayIndexToInt(a,getLimits()));
		}
		return translated;
	}

	vector<array<indexType,dimensions>> getNeighbours(array<indexType,dimensions> arr, const array<indexType,dimensions>& limits, bool center = true,  size_t currentDim = 0){
		       vector<array<indexType,dimensions>> neigh {};
		       if(currentDim+1 == dimensions){
		           auto arrMinus = arr;
		           auto arrPlus = arr;
		           if(!center){
		               neigh.emplace_back(arr);
		           }
		           arrPlus[currentDim]  = (arr[currentDim] - 1 + limits[currentDim]) % limits[currentDim];
		           arrMinus[currentDim] = (arr[currentDim] + 1) % limits[currentDim];
		           neigh.emplace_back(arrMinus);
		           neigh.emplace_back(arrPlus);
		       }
		       else{
		           auto arrMinus = arr;
		           auto arrPlus = arr;
		           arrPlus[currentDim]  = (arr[currentDim] - 1 + limits[currentDim]) % limits[currentDim];
		           arrMinus[currentDim] = (arr[currentDim] + 1) % limits[currentDim];
		           vector<array<indexType,dimensions>> vec {};
		           vec =  getNeighbours(arr,limits, center, currentDim + 1);
		           neigh.insert(neigh.end(),vec.begin(),vec.end());

		           vec =  getNeighbours(arrMinus,limits, false, currentDim + 1);
		           neigh.insert(neigh.end(),vec.begin(),vec.end());

		           vec =  getNeighbours(arrPlus,limits, false, currentDim + 1);
		           neigh.insert(neigh.end(),vec.begin(),vec.end());
		       }
		    return neigh;
		}

	vector<pair<indexType,shared_ptr<Voxel<dimensions>>>> freeVoxels;
	vector<unique_ptr<GridElement<dimensions>>> elements;
	std::map<indexType, vector<indexType>> neighbours;
	double coverage;
	std::vector<std::mt19937> generators;
};


#endif /* GRID_HPP_ */
