/*
 * gridElement.hpp
 *
 *  Created on: Mar 12, 2019
 *      Author: rsteam
 */

#ifndef GRIDELEMENT_HPP_
#define GRIDELEMENT_HPP_

#include "helpers.hpp"
#include "cuboid.hpp"
#include <algorithm>
#include <array>
#include <memory>
#include <random>

enum class GridElementStatus {
	empty, blocked, filled, candidate
};

template<int dimensions>
class GridElement{
public:
	GridElement() :
			cuboid(), position( {}), status(GridElementStatus::empty), volume(1){
	}
	explicit GridElement(std::array<indexType, dimensions> pos) :
			status(GridElementStatus::empty), cuboid(), position(pos), volume(1){
		auto topLevelVoxel = std::make_shared<Voxel<dimensions>>(position);
		voxels.push_back(topLevelVoxel);
		resetBoundingBox();
	}
	const std::array<indexType, dimensions> &getPosition() const {
		return position;
	}
	const std::vector<std::shared_ptr<Voxel<dimensions>>> getVoxels() {
		return voxels;
	}
	void clearVoxels() {
		voxels.clear();
	}
	void resetVoxels() {
		voxels.clear();
		auto topLevelVoxel = std::make_shared<Voxel<dimensions>>(position);
		voxels.push_back(topLevelVoxel);
	}
	void resetBoundingBox(){
		auto bBox = std::make_shared<Voxel<dimensions>>(position);
		for (size_t a = 0; a < dimensions; a++) {
			bBox->position[a]  += fixedPointOne;
			bBox->dimension[a] = fixedPointZero;
		}
		boundingBox = bBox;
		boundingRatio = 1;
	}
	void splitVoxels(Cuboid<dimensions>& cuboid, array<indexType,dimensions> limits) {
		if (status == GridElementStatus::empty) {
			std::vector<std::shared_ptr<Voxel<dimensions>>> voxelsAfterSplit {};
			for (auto voxel : voxels) {
				auto vector = voxel->splitVoxel(cuboid.getPosition(), limits);
				voxelsAfterSplit.insert(voxelsAfterSplit.end(), vector.begin(), vector.end());
			}
			voxels = voxelsAfterSplit;
			if (voxels.empty()) {
				status = GridElementStatus::blocked;
			}
		}
	}
	GridElementStatus status;
	Cuboid<dimensions> cuboid;
	std::shared_ptr<Voxel<dimensions>> boundingBox;
	double boundingRatio;
private:
	std::vector<std::shared_ptr<Voxel<dimensions>>> voxels;
	std::array<indexType , dimensions> position;
	double volume;
public:
	double getVolume() const {
		return volume;
	}
	std::shared_ptr<Voxel<dimensions>> selectRandomVoxel(std::mt19937 generator){
		std::vector<double> probsVector {};
		for(auto vox : voxels){
			probsVector.push_back(vox->getVolume());
		}
		std::discrete_distribution<> discreteDistribution(probsVector.begin(), probsVector.end());
		auto iter = discreteDistribution(generator);
		return voxels[iter];
	}
};



#endif /* GRIDELEMENT_HPP_ */
